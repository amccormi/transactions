from django.conf.urls import url
from django.contrib import admin
from money import views
from money import views as core_views
from django.contrib.auth import views as auth_views
from graphene_django.views import GraphQLView


# from product_inventory_manager.schema import schema

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/$', auth_views.login, {'template_name': 'transaction/login.html'},name = 'login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/'},name = 'logout'),
    url(r'^graphql', GraphQLView.as_view(graphiql=True), name="graphql"),
    url(r'^$', views.home, name='home'),
    url(r'^transaction/new/$', views.transaction_new, name='transaction_new'),
    url(r'^transaction/(?P<pk>\d+)/edit/$', views.transaction_edit, name='transaction_edit'),
    url(r'^signup/$', core_views.signup, name='signup'),
]