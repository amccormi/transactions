import graphene

import money.schema 


class Query(money.schema.Query, graphene.ObjectType):
# This class extends all abstract apps level Queries and graphene.ObjectType
	pass

schema = graphene.Schema(query=Query)