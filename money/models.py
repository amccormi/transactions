# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
# an expense should be comprised of a name, date, amount, and category at least
   
class Transaction(models.Model):
	name = models.CharField(max_length=150)
	date = models.DateField(null=True)
	amount = models.TextField()
	category = models.TextField()
	

	# REASONS = (
	#     ('ns', 'New Stock'),
	#     ('ur', 'Usable Return'),
	#     ('nr', 'Unusable Return'),
	# )


	# reason = models.CharField(max_length=2, choices=REASONS, blank=True, default='ns', help_text='Reason for transaction')

	def __str__(self):
        	return self.name
