import graphene

from graphene_django.types import DjangoObjectType

from .models import Transaction 

class TransactionType(DjangoObjectType):
    class Meta:
        model = Transaction

class Query(graphene.AbstractType):
        transactions = graphene.Field(TransactionType,
                              id=graphene.Int(),
                              name=graphene.String(),
                              date=graphene.String(),
                              amount=graphene.String(),
                              category=graphene.String())
        all_transactions = graphene.List(TransactionType)

        def resolve_all_transactions(self, info, **kwargs):
            return Transaction.objects.all()

        def resolve_transactions(self, info, **kwargs):
            id = kwargs.get('id')
            name = kwargs.get('name')
            date = kwargs.get('date')
            amount = kwargs.get('amount')
            category = kwargs.get('category')

            if id is not None:
                return Transaction.objects.get(pk=id)

            if name is not None:
                return Transaction.objects.get(name=name)
            
            if date is not None:
                return Transaction.objects.get(date=date)
            
            if amount is not None:
                return Transaction.objects.get(amount=amount)

            if category is not None:
                return Transaction.objects.get(category=category)

            return None