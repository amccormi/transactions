# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render

from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.utils import timezone
from .models import Transaction
from django.shortcuts import render, get_object_or_404, redirect
from .forms import TransactionForm
from .forms import LoginForm
from django.template import RequestContext
import time
import json
from django.core.serializers.json import DjangoJSONEncoder


from django.shortcuts import redirect
# Create your views here.
def home(request):
    transactions = Transaction.objects.order_by('-date')
    return render(request, 'transaction/dashboard.html', {'transactions': transactions})


def transaction_new(request):
    if request.method == "POST":
        form = TransactionForm(request.POST)
        if form.is_valid():
            transaction = form.save(commit=False)
            transaction.author = request.user
            transaction.published_date__lted_date = timezone.now()
            transaction.save()
            return redirect('home')
    else:
        form = TransactionForm()
    return render(request, 'transaction/transaction_edit.html', {'form': form})

def transaction_edit(request):
    if request.method == "POST":
        form = TransactionForm(request.POST, instance=transaction)
        if form.is_valid():
            transaction = form.save(commit=False)
            transaction.author = request.user
            transaction.published_date = timezone.now()
            transaction.save()
            return redirect('home')
    else:
        form = TransactionForm(instance=transaction)
    return render(request, 'transaction/transaction_edit.html', {'form': form})

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = UserCreationForm()
    return render(request, 'transaction/signup.html', {'form': form})

def newlogin(request):
    posts = Post.objects.all()
    print(User.objects.all().values_list('username','password'))
    if request.method == 'POST':
        user = User.objects.filter(username=request.POST.get('username'), password=request.POST.get('password'))
        login_form = LoginForm(request.POST)
        print(user)
        if login_form.is_valid():
            return render(request, 'screen/home_page.html', {'posts':posts})

    else:
        login_form = LoginForm()
        return render(request, 'transaction/login.html', {'form': login_form})

